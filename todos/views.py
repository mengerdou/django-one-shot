from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from todos.models import TodoItem, TodoList
from django.urls import reverse_lazy

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todo_list/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todo_list/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todo_list/new.html"
    fields = ["name"]
    # success_url = reverse_lazy("list_todos")

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todo_list/edit.html"

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todo_list/delete.html"
    success_url = reverse_lazy("list_todos")


class ItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_list/itemnew.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class ItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todo_list/itemedit.html"

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])
